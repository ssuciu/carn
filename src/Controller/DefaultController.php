<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route("/carn", name="app_homepage")
     */
    public function carn()
    {
        return $this->render('base.html.twig');
    }
}
