<?php

namespace App\Form;

use App\Entity\User;
use App\Validator\PasswordConfirm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Parolele trebuie sa coincida.',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Campul de confirmat parola nu poate sa fie gol.'
                    ]),
                    new Length([
                        'minMessage' => 'Campul de confirmat parola trebuie sa aiba cel putin 5 caractere',
                        'min' => 5
                    ])
                ]
            ])
            ->add('termsAcceptedAt', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Trebuie sa acceptati termenii si conditiile.'
                    ])
                ]
            ])
            ->add('privacyPolicyAcceptedAt', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Trebuie sa acceptati politica de confidentialitate.'
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
